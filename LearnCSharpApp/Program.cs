﻿using System.Net;
using System.Xml.Linq;

namespace LearnCSharpApp
{
    internal class Program
    {
        static void DisplayWelcome(string name)
        {
            Console.WriteLine("Welcome " + name);
        }
        static void Main(string[] args)
        {
            /*            Console.WriteLine("Hello, what's your name?");
                        string name = Console.ReadLine();
                        DisplayWelcome(name);*/

            DisplayAddress();


        }

        static void DisplayAddress()
        {

            string addressName = "John Doe";
            string address = "1234 Main St";
            string apt = "4";
            string city = "Detroit";
            string state = "MI";
            string zip = "48226";

            Console.WriteLine($"{addressName}");
            Console.WriteLine($"{address} " + $"Apt #{apt}");
            Console.WriteLine($"{city}" + ", " + $"{state}");
            Console.WriteLine($"{zip}");
        }
    }
}

